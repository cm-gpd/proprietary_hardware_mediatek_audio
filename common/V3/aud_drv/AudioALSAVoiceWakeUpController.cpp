#include "AudioALSAVoiceWakeUpController.h"

#include "AudioLock.h"
#include "AudioALSADriverUtility.h"

#include "AudioMTKHeadsetMessager.h"

#include "AudioCustParamClient.h"
#include "AudioUtility.h"
#include "AudioALSAHardwareResourceManager.h"
#include "AudioALSADeviceConfigManager.h"
#include "AudioALSADeviceParser.h"
#include "AudioALSAStreamManager.h"
#include <linux/vow.h>

#ifdef MTK_VOW_BARGE_IN_SUPPORT
#include "AudioALSADeviceParser.h"
#endif

#ifdef MTK_AUDIO_SCP_SUPPORT
#include <audio_task.h>
#include <audio_messenger_ipi.h>
#endif

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "AudioALSAVoiceWakeUpController"
#define VOW_POWER_ON_SLEEP_MS 50
namespace android {

AudioALSAVoiceWakeUpController *AudioALSAVoiceWakeUpController::mAudioALSAVoiceWakeUpController = NULL;
AudioALSAVoiceWakeUpController *AudioALSAVoiceWakeUpController::getInstance() {
    static AudioLock mGetInstanceLock;
    AL_AUTOLOCK(mGetInstanceLock);

    if (mAudioALSAVoiceWakeUpController == NULL) {
        mAudioALSAVoiceWakeUpController = new AudioALSAVoiceWakeUpController();
    }
    ASSERT(mAudioALSAVoiceWakeUpController != NULL);
    return mAudioALSAVoiceWakeUpController;
}

/*==============================================================================
 *                     Callback Function
 *============================================================================*/
#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT)
void callbackVowXmlChanged(AppHandle *appHandle, const char *audioTypeName)
{
    AppOps *appOps = appOpsGetInstance();
    ALOGD("+%s(), audioType = %s", __FUNCTION__, audioTypeName);

    if (appOps == NULL) {
        ALOGE("Error %s %d", __FUNCTION__, __LINE__);
        ASSERT(0);
        return;
    }
    // reload XML file
    if (appOps->appHandleReloadAudioType(appHandle, audioTypeName) == APP_ERROR) {
        ALOGE("%s(), Reload xml fail!(audioType = %s)", __FUNCTION__, audioTypeName);
    } else {
        if (strcmp(audioTypeName, "VOW") == 0) {
             AudioALSAVoiceWakeUpController::getInstance()->updateVOWCustParam();
        }
    }
}
#endif

AudioALSAVoiceWakeUpController::AudioALSAVoiceWakeUpController() :
    mDebug_Enable(false),
    mMixer(AudioALSADriverUtility::getInstance()->getMixer()),
    mPcm(NULL),
    mEnable(false),
#ifdef MTK_VOW_BARGE_IN_SUPPORT
    mBargeInEnable(false),
    mBargeInPcm(NULL),
#endif
    mIsUseHeadsetMic(false),
    mHardwareResourceManager(AudioALSAHardwareResourceManager::getInstance()),
    mIsNeedToUpdateParamToKernel(true) {
    ALOGD("%s()", __FUNCTION__);

    mHandsetMicMode = mHardwareResourceManager->getPhoneMicMode();
    mHeadsetMicMode = mHardwareResourceManager->getHeadsetMicMode();

#ifdef MTK_VOW_BARGE_IN_SUPPORT
    mIsSpeakerPlaying = false;
#endif
    mFd_vow = 0;
    mFd_vow = ::open("/dev/vow", O_RDONLY);

    //mVOWCaptureDataProvider = AudioALSACaptureDataProviderVOW::getInstance();
    mDeviceConfigManager = AudioALSADeviceConfigManager::getInstance();

    stream_attribute_target = new stream_attribute_t;
    memset(stream_attribute_target, 0, sizeof(stream_attribute_t));
    stream_attribute_target->input_source = AUDIO_SOURCE_HOTWORD;
    // Init input stream attribute here
    stream_attribute_target->audio_mode = AUDIO_MODE_NORMAL; // set mode to stream attribute for mic gain setting
    stream_attribute_target->output_devices = AUDIO_DEVICE_NONE; // set output devices to stream attribute for mic gain setting and BesRecord parameter
    hDumyReadThread = 0;
    mDumpReadStart = false;
    mFd_dnn = -1;
    // BesRecordInfo

    besrecord_info_struct_t besrecord;
    //native_preprocess_info_struct_t nativePreprocess_Info;
    memset(&besrecord, 0, sizeof(besrecord_info_struct_t));
    stream_attribute_target->BesRecord_Info = besrecord;

    stream_attribute_target->BesRecord_Info.besrecord_enable = false; // default set besrecord off
    stream_attribute_target->BesRecord_Info.besrecord_bypass_dualmicprocess = false;  // bypass dual MIC preprocess
    stream_attribute_target->NativePreprocess_Info.PreProcessEffect_Update = false;
    stream_attribute_target->sample_rate = 16000;
    stream_attribute_target->audio_format = AUDIO_FORMAT_PCM_16_BIT;
    //stream_attribute_target->audio_channel_mask = AUDIO_CHANNEL_IN_MONO;
    //mStreamAttributeSource.num_channels = popcount(mStreamAttributeSource.audio_channel_mask);
    //mStreamAttributeSource.sample_rate = 16000;
    ALOGD("%s() , stream_attribute_target->BesRecord_Info.besrecord_enable %d", __FUNCTION__, stream_attribute_target->BesRecord_Info.besrecord_enable);
#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT)
    /* Init AppHandle */
    AppOps *appOps = appOpsGetInstance();
    if (appOps == NULL) {
        ALOGE("Error %s %d", __FUNCTION__, __LINE__);
        ASSERT(0);
    } else {
        mAppHandle = appOps->appHandleGetInstance();
        /* XML changed callback process */
        appOps->appHandleRegXmlChangedCb(mAppHandle, callbackVowXmlChanged);
    }
#endif
    updateParamToKernel();
}

AudioALSAVoiceWakeUpController::~AudioALSAVoiceWakeUpController() {
    if (mFd_vow > 0) {
        ::close(mFd_vow);
        mFd_vow = 0;
    }
    ALOGD("%s()", __FUNCTION__);
}

status_t AudioALSAVoiceWakeUpController::SeamlessRecordEnable() {
    int ret = NO_ERROR;

    AL_AUTOLOCK(mSeamlessLock);
    ALOGD("+%s()", __FUNCTION__);
    if (mFd_dnn < 0) {
        mFd_dnn = open("/dev/vow", O_RDONLY);
    }
    if (mFd_dnn < 0) {
        ALOGI("open device fail!%s\n", strerror(errno));
    }

    ret = ::ioctl(mFd_dnn, VOW_SET_CONTROL, (unsigned int)VOWControlCmd_EnableSeamlessRecord);
    if (ret != 0) {
        ALOGE("%s(), VOWControlCmd_EnableHotwordRecord error, ret = %d", __FUNCTION__, ret);
    }

    ALOGD("-%s()", __FUNCTION__);
    return NO_ERROR;
}

bool AudioALSAVoiceWakeUpController::getVoiceWakeUpEnable() {
    AL_AUTOLOCK(mLock);
    return mEnable;
}


status_t AudioALSAVoiceWakeUpController::setVoiceWakeUpEnable(const bool enable) {
    int ret = -1;

    ALOGD("+%s(), mEnable: %d => %d, mIsUseHeadsetMic = %d, mHeadsetMicMode = %d, mHandsetMicMode = %d",
          __FUNCTION__, mEnable, enable, mIsUseHeadsetMic, mHeadsetMicMode, mHandsetMicMode);
    AL_AUTOLOCK(mLock);

    if (mEnable == enable) {
        ALOGW("-%s(), enable(%d) == mEnable(%d), return", __FUNCTION__, enable, mEnable);
        return INVALID_OPERATION;
    }

    if (enable == true) {
        unsigned int mic_type = 0;
        unsigned int mtkif_type = 0;

        /* Switch SCP Dynamic Object */
#ifdef MTK_AUDIO_SCP_SUPPORT
        /* Load task scene when opening */
        audio_load_task_scene(TASK_SCENE_VOW);
#endif

        setVoiceWakeUpDebugDumpEnable(true);

        updateParamToKernel();
#if defined(MTK_AUDIO_KS) && defined(MTK_VOW_SUPPORT)
        struct pcm_config config;

        memset(&config, 0, sizeof(config));
        config.channels = 1;
        config.rate = 16000;
        config.period_size = 1024;
        config.period_count = 2;
        config.format = PCM_FORMAT_S16_LE;
        config.stop_threshold = ~(0U);

        int cardIndex = AudioALSADeviceParser::getInstance()->GetCardIndexByString(keypcmVOWCapture);
        int pcmIndex = AudioALSADeviceParser::getInstance()->GetPcmIndexByString(keypcmVOWCapture);


        if (mIsUseHeadsetMic) {
            mDeviceConfigManager->ApplyDeviceTurnonSequenceByName(AUDIO_DEVICE_HEADSET_VOW_MIC);
            if (mHeadsetMicMode == AUDIO_MIC_MODE_ACC) {
                ALOGD("VOW HEADSET ACC");
                mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_ACCMODE);
            } else if (mHeadsetMicMode == AUDIO_MIC_MODE_DCC) {
                ALOGD("VOW HEADSET DCC");
                mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_DCCMODE);
            } else if (mHeadsetMicMode == AUDIO_MIC_MODE_DCCECMDIFF) {
                ALOGD("VOW HEADSET DCC DIFF");
                mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_DCCECMDIFFMODE);
            } else if (mHeadsetMicMode == AUDIO_MIC_MODE_DCCECMSINGLE) {
                ALOGD("VOW HEADSET DCC SINGLE");
                mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_DCCECMSINGLEMODE);
            } else {
                ALOGD("%s(), vow mic type error, mHeadsetMicMode=%d", __FUNCTION__, mHeadsetMicMode);
            }
        } else {
            if (IsAudioSupportFeature(AUDIO_SUPPORT_DMIC)) {
                mDeviceConfigManager->ApplyDeviceTurnonSequenceByName(AUDIO_DEVICE_BUILTIN_MIC_VOW_MIC);
                if (mHandsetMicMode == AUDIO_MIC_MODE_DMIC_LP) {
                    ALOGD("VOW DMIC");
                    mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_DMICLPMODE);
                } else if (mHandsetMicMode == AUDIO_MIC_MODE_DMIC) {
                    ALOGD("VOW DMIC LP");
                    mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_DMICMODE);
                } else {
                    ALOGD("%s(), vow mic type error, mHandsetMicMode=%d", __FUNCTION__, mHandsetMicMode);
                }
            } else {
                mDeviceConfigManager->ApplyDeviceTurnonSequenceByName(AUDIO_DEVICE_BUILTIN_MIC_VOW_MIC);
                if (mHandsetMicMode == AUDIO_MIC_MODE_ACC) {
                    ALOGD("VOW MAIN MIC ACC");
                    mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_ACCMODE);
                } else if (mHandsetMicMode == AUDIO_MIC_MODE_DCC) {
                    ALOGD("VOW MAIN MIC DCC");
                    mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_DCCMODE);
                } else if (mHandsetMicMode == AUDIO_MIC_MODE_DCCECMDIFF) {
                    ALOGD("VOW MAIN MIC DCC DIFF");
                    mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_DCCECMDIFFMODE);
                } else if (mHandsetMicMode == AUDIO_MIC_MODE_DCCECMSINGLE) {
                    ALOGD("VOW MAIN MIC DCC SINGLE");
                    mDeviceConfigManager->ApplyDeviceSettingByName(VOW_MIC_TYPE_DCCECMSINGLEMODE);
                } else {
                    ALOGD("%s(), vow mic type error, mHandsetMicMode=%d", __FUNCTION__, mHandsetMicMode);
                }
            }
        }

        mPcm = pcm_open(cardIndex, pcmIndex , PCM_IN, &config);
        if (mPcm == NULL || pcm_is_ready(mPcm) == false) {
            ALOGE("%s(), Unable to open pcm device %u (%s)", __FUNCTION__, pcmIndex, pcm_get_error(mPcm));
        } else {
            if (pcm_start(mPcm)) {
                ALOGE("%s(), pcm_start %p fail due to %s", __FUNCTION__, mPcm, pcm_get_error(mPcm));
            }
        }
#else
        //set input MIC type
        if (mIsUseHeadsetMic) {
            //use headset mic
            if (mHeadsetMicMode == AUDIO_MIC_MODE_ACC) {
                if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HeadsetMIC")) {
                    ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value HeadsetMIC");
                }
            } else if (mHeadsetMicMode == AUDIO_MIC_MODE_DCC) {
                if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HeadsetMIC_DCC")) {
                    ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value HeadsetMIC_DCC");
                }
            } else if (mHeadsetMicMode == AUDIO_MIC_MODE_DCCECMDIFF) {
                if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HeadsetMIC_DCCECM")) {
                    ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value AUDIO_MIC_MODE_DCCECMDIFF");
                }
            } else if (mHeadsetMicMode == AUDIO_MIC_MODE_DCCECMSINGLE) {
                if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HeadsetMIC_DCCECM")) {
                    ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value AUDIO_MIC_MODE_DCCECMSINGLE");
                }
            } else {
                if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HeadsetMIC")) {
                    ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value HeadsetMIC");
                }
            }
        } else {
            //DMIC device
            if (IsAudioSupportFeature(AUDIO_SUPPORT_DMIC)) {
                if (mHandsetMicMode == AUDIO_MIC_MODE_DMIC_LP) {
                    if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HandsetDMIC_800K")) {
                        ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value HandsetDMIC_800K");
                    }
                } else if(mHandsetMicMode == AUDIO_MIC_MODE_DMIC_VENDOR01) {
                    if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HandsetDMIC_VENDOR01")) {
                        ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value HandsetDMIC_VENDOR01");
                    }
                } else { //normal DMIC
                    if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HandsetDMIC")) {
                        ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value HandsetDMIC");
                    }
                }
            } else { //analog MIC device
                if (mHandsetMicMode == AUDIO_MIC_MODE_ACC) {
                    if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HandsetAMIC")) {
                        ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value HandsetAMIC");
                    }
                } else if (mHandsetMicMode == AUDIO_MIC_MODE_DCC) { //DCC mems mic
                    if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HandsetAMIC_DCC")) {
                        ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value HandsetAMIC_DCC");
                    }
                } else if (mHandsetMicMode == AUDIO_MIC_MODE_DCCECMDIFF) { //DCC ecm mic
                    if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HandsetAMIC_DCCECM")) {
                        ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value AUDIO_MIC_MODE_DCCECMDIFF");
                    }
                } else if (mHandsetMicMode == AUDIO_MIC_MODE_DCCECMSINGLE) { //DCC ecm mic
                    if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HandsetAMIC_DCCECM")) {
                        ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value AUDIO_MIC_MODE_DCCECMSINGLE");
                    }
                } else {
                    if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_MIC_Type_Select"), "HandsetAMIC")) {
                        ALOGE("Error: Audio_Vow_MIC_Type_Select invalid value HandsetAMIC");
                    }
                }
            }
        }


        if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_ADC_Func_Switch"), "On")) {
            ALOGE("Error: Audio_Vow_ADC_Func_Switch invalid value");
        }

        usleep(VOW_POWER_ON_SLEEP_MS * 1000);

        if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_Digital_Func_Switch"), "On")) {
            ALOGE("Error: Audio_Vow_Digital_Func_Switch invalid value");
        }
#endif
        mic_type = getVOWMicType();
        switch (mic_type) {
            case AUDIO_MIC_MODE_DMIC:
                mtkif_type = VOW_MTKIF_DMIC;
                break;
            case AUDIO_MIC_MODE_DMIC_LP:
            case AUDIO_MIC_MODE_DMIC_VENDOR01:
                mtkif_type = VOW_MTKIF_DMIC_LP;
                break;
            case AUDIO_MIC_MODE_ACC:
            case AUDIO_MIC_MODE_DCC:
            case AUDIO_MIC_MODE_DCCECMDIFF:
            case AUDIO_MIC_MODE_DCCECMSINGLE:
                mtkif_type = VOW_MTKIF_AMIC;
                break;
            default:
                ALOGI("err: get wrong mic type, need check!\n");
                ASSERT(0);
                break;
        }
        ret = ::ioctl(mFd_vow, VOW_RECOG_ENABLE, mtkif_type);
        if (ret != 0) {
            ALOGE("%s(), VOW_RECOG_ENABLE error, ret = %d", __FUNCTION__, ret);
        }
#ifdef MTK_VOW_BARGE_IN_SUPPORT
        if (mIsSpeakerPlaying == true) {
            setBargeInEnable(true);
        }
#endif
    } else {
        setVoiceWakeUpDebugDumpEnable(false);
        ret = ::ioctl(mFd_vow, VOW_RECOG_DISABLE, VOW_MTKIF_NONE);
        if (ret != 0) {
            ALOGE("%s(), VOW_RECOG_DISABLE error, ret = %d", __FUNCTION__, ret);
        }
#if defined(MTK_AUDIO_KS)
        if (mPcm) {
            pcm_stop(mPcm);
            pcm_close(mPcm);
            mPcm = NULL;
        }

        if (mIsUseHeadsetMic) {
            mDeviceConfigManager->ApplyDeviceTurnoffSequenceByName(AUDIO_DEVICE_HEADSET_VOW_MIC);
        } else {
            if (IsAudioSupportFeature(AUDIO_SUPPORT_DMIC)) {
                mDeviceConfigManager->ApplyDeviceTurnoffSequenceByName(AUDIO_DEVICE_BUILTIN_MIC_VOW_MIC);
            } else {
                mDeviceConfigManager->ApplyDeviceTurnoffSequenceByName(AUDIO_DEVICE_BUILTIN_MIC_VOW_MIC);
            }
        }
#else
        if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_Digital_Func_Switch"), "Off")) {
            ALOGE("Error: Audio_Vow_Digital_Func_Switch invalid value");
        }


        if (mixer_ctl_set_enum_by_string(mixer_get_ctl_by_name(mMixer, "Audio_Vow_ADC_Func_Switch"), "Off")) {
            ALOGE("Error: Audio_Vow_ADC_Func_Switch invalid value");
        }
#endif
#ifdef MTK_VOW_BARGE_IN_SUPPORT
        if (mBargeInEnable) {
            setBargeInEnable(false);
        }
#endif
    }

    mEnable = enable;

    ALOGD("-%s()", __FUNCTION__);
    return NO_ERROR;
}


status_t AudioALSAVoiceWakeUpController::updateDeviceInfoForVoiceWakeUp() {
    ALOGD("+%s(), mIsUseHeadsetMic = %d", __FUNCTION__, mIsUseHeadsetMic);

    bool bIsUseHeadsetMic = AudioALSAStreamManager::getInstance()->getDeviceConnectionState(AUDIO_DEVICE_OUT_WIRED_HEADSET);

    if (bIsUseHeadsetMic != mIsUseHeadsetMic) {
        if (mEnable == false) {
            mIsUseHeadsetMic = bIsUseHeadsetMic;
        } else {
            setVoiceWakeUpEnable(false);
            mIsUseHeadsetMic = bIsUseHeadsetMic;
            setVoiceWakeUpEnable(true);
        }
    }

    ALOGD("-%s(), mIsUseHeadsetMic = %d, bIsUseHeadsetMic = %d", __FUNCTION__, mIsUseHeadsetMic, bIsUseHeadsetMic);
    return NO_ERROR;
}


status_t AudioALSAVoiceWakeUpController::updateVOWCustParam() {
    AL_AUTOLOCK(mLock);

    mIsNeedToUpdateParamToKernel = true;
    return NO_ERROR;
}

unsigned int AudioALSAVoiceWakeUpController::getVOWMicType() {

    if (mIsUseHeadsetMic) {
        /* Headset */
        return mHeadsetMicMode;
    } else {
        /* DMIC or Handset */
        return mHandsetMicMode;
    }
}

status_t AudioALSAVoiceWakeUpController::updateParamToKernel() {
    if (mIsNeedToUpdateParamToKernel == true) {
        mIsNeedToUpdateParamToKernel = false;

        int mVOW_CFG0 = 0;
        int mVOW_CFG1 = 0;
        int mVOW_CFG2 = 0;
        int mVOW_CFG3 = 0;
        int mVOW_CFG4 = 0;
        int mVOW_CFG5 = 0;
        int mVOW_Periodic_OnOff = 0;

#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT)
        int cust_param[20] = {0}; // all elements 0
        struct mixer_ctl *ctl;

        /* Get the vow common parameter from XML */
        AppOps *appOps = appOpsGetInstance();
        if (appOps == NULL) {
            ALOGE("Error %s %d", __FUNCTION__, __LINE__);
            ASSERT(0);
            return UNKNOWN_ERROR;
        } else {
            AudioType *audioType;
            // define xml names
            char audioTypeName[] = "VOW";
            audioType = appOps->appHandleGetAudioTypeByName(mAppHandle, "VOW");
            if (!audioType) {
                ALOGE("%s(), get audioType fail, audioTypeName = %s",
                      __FUNCTION__, audioTypeName);
                return BAD_VALUE;
            }
            Param *param = 0;
            ALOGD("%s(), get vow param from XML:%p", __FUNCTION__, audioType);

            std::string paramCommonPath = "VOW,VOW_common";

            ParamUnit *paramUnit;
            paramUnit = appOps->audioTypeGetParamUnit(audioType, paramCommonPath.c_str());
            if (!paramUnit) {
                ALOGD("%s(), get paramUnit fail, paramPath = %s, use common",
                      __FUNCTION__,
                      paramCommonPath.c_str());
                return BAD_VALUE;
            }
            // Read lock
            appOps->audioTypeReadLock(audioType, __FUNCTION__);

            param = appOps->paramUnitGetParamByName(paramUnit, "Par_01");
            ASSERT(param);
            cust_param[0] = *(int *)param->data;
            param = appOps->paramUnitGetParamByName(paramUnit, "Par_02");
            ASSERT(param);
            cust_param[1] = *(int *)param->data;
            param = appOps->paramUnitGetParamByName(paramUnit, "Par_03");
            ASSERT(param);
            cust_param[2] = *(int *)param->data;
            param = appOps->paramUnitGetParamByName(paramUnit, "Par_04");
            ASSERT(param);
            cust_param[3] = *(int *)param->data;
            param = appOps->paramUnitGetParamByName(paramUnit, "Par_05");
            ASSERT(param);
            cust_param[4] = *(int *)param->data;
            param = appOps->paramUnitGetParamByName(paramUnit, "Par_06");
            ASSERT(param);
            cust_param[5] = *(int *)param->data;
            param = appOps->paramUnitGetParamByName(paramUnit, "Par_07");
            ASSERT(param);
            cust_param[6] = *(int *)param->data;
            param = appOps->paramUnitGetParamByName(paramUnit, "Par_08");
            ASSERT(param);
            cust_param[7] = *(int *)param->data;
            param = appOps->paramUnitGetParamByName(paramUnit, "Par_09");
            ASSERT(param);
            cust_param[8] = *(int *)param->data;
            param = appOps->paramUnitGetParamByName(paramUnit, "Par_10");
            ASSERT(param);
            cust_param[9] = *(int *)param->data;

            // Unlock
            appOps->audioTypeUnlock(audioType);
        }

        ctl = mixer_get_ctl_by_name(mMixer, "Audio VOWCFG4 Data");
        mVOW_CFG4 = mixer_ctl_get_value(ctl, 0);
        ALOGD("%s(), mVOW_CFG4 load = 0x%x", __FUNCTION__, mVOW_CFG4);
        mVOW_CFG0 = 0x0000;
        mVOW_CFG1 = 0x0000;
        mVOW_CFG2 = ((cust_param[5] & 0x0007) << 12) |
                    ((cust_param[6] & 0x0007) << 8)  |
                    ((cust_param[7] & 0x0007) << 4)  |
                    ((cust_param[8] & 0x0007));
        mVOW_CFG3 = ((cust_param[0] & 0x000f) << 12) |
                    ((cust_param[1] & 0x000f) << 8)  |
                    ((cust_param[2] & 0x000f) << 4)  |
                    ((cust_param[3] & 0x000f));
        mVOW_CFG4 &= 0xFFF0;
        mVOW_CFG4 |= cust_param[4];
        mVOW_CFG5 = 0x0001;
        mVOW_Periodic_OnOff = cust_param[9];
#endif
        ALOGD("%s(), CFG0=0x%x, CFG1=0x%x, CFG2=0x%x, CF3=0x%x, CFG4=0x%x, CFG5=0x%x, Perio=0x%x",
              __FUNCTION__,
              mVOW_CFG0,
              mVOW_CFG1,
              mVOW_CFG2,
              mVOW_CFG3,
              mVOW_CFG4,
              mVOW_CFG5,
              mVOW_Periodic_OnOff);

        if (mixer_ctl_set_value(mixer_get_ctl_by_name(mMixer, "Audio VOWCFG0 Data"), 0, mVOW_CFG0)) {
            ALOGE("Error: Audio VOWCFG0 Data invalid value");
        }

        if (mixer_ctl_set_value(mixer_get_ctl_by_name(mMixer, "Audio VOWCFG1 Data"), 0, mVOW_CFG1)) {
            ALOGE("Error: Audio VOWCFG1 Data invalid value");
        }

        if (mixer_ctl_set_value(mixer_get_ctl_by_name(mMixer, "Audio VOWCFG2 Data"), 0, mVOW_CFG2)) {
            ALOGE("Error: Audio VOWCFG2 Data invalid value");
        }

        if (mixer_ctl_set_value(mixer_get_ctl_by_name(mMixer, "Audio VOWCFG3 Data"), 0, mVOW_CFG3)) {
            ALOGE("Error: Audio VOWCFG3 Data invalid value");
        }

        if (mixer_ctl_set_value(mixer_get_ctl_by_name(mMixer, "Audio VOWCFG4 Data"), 0, mVOW_CFG4)) {
            ALOGE("Error: Audio VOWCFG4 Data invalid value");
        }

        if (mixer_ctl_set_value(mixer_get_ctl_by_name(mMixer, "Audio VOWCFG5 Data"), 0, mVOW_CFG5)) {
            ALOGE("Error: Audio VOWCFG5 Data invalid value");
        }

        if (mixer_ctl_set_value(mixer_get_ctl_by_name(mMixer, "Audio_VOW_Periodic"), 0, mVOW_Periodic_OnOff)) {
            ALOGE("Error: Audio VOW Periodic On Off Data invalid value");
        }

    }
    return NO_ERROR;
}

void *AudioALSAVoiceWakeUpController::dumyReadThread(void *arg) {
    AudioALSAVoiceWakeUpController *pWakeupController = static_cast<AudioALSAVoiceWakeUpController *>(arg);
    short *pbuf = new short[160];

    if (arg == 0) {
       ALOGD("%s(), Error, arg=NULL", __FUNCTION__);
       goto exit;
    }
    if (pWakeupController == 0) {
       ALOGD("%s(), Error, pWakeupController=NULL", __FUNCTION__);
       goto exit;
    }
    if (pWakeupController->hDumyReadThread == 0) {
        goto exit;
    }
    if (!pWakeupController->mCaptureHandler) {
        ALOGD("%s(), Error, mCaptureHandler not here", __FUNCTION__);
        goto exit;
    }
    ALOGD("+%s(), dumyReadThread end, arg=%p", __FUNCTION__, arg);
    while(pWakeupController->mDumpReadStart) {
        pWakeupController->mCaptureHandler->read(&pbuf[0], 320);
        ALOGV("read once");
    }
exit:
    delete[] pbuf;
    ALOGD("-%s(), dumyReadThread end", __FUNCTION__);
    pthread_exit(NULL);
    return NULL;
}

status_t AudioALSAVoiceWakeUpController::setVoiceWakeUpDebugDumpEnable(const bool enable) {
    ALOGD("+%s(), mDebug_Enable: %d => %d", __FUNCTION__, mDebug_Enable, enable);
    status_t status;
    int ret = -1;

    AL_AUTOLOCK(mDebugDumpLock);
    if (mDebug_Enable == enable) {
        ALOGW("-%s(), enable(%d) == mDebug_Enable(%d), return", __FUNCTION__, enable, mDebug_Enable);
        return INVALID_OPERATION;
    }

    char value[PROPERTY_VALUE_MAX];
    property_get(streamin_propty, value, "0");
    int bflag = atoi(value);

    if (bflag && enable) {
        if (!mDebug_Enable) {
            //enable VOW debug dump
            mCaptureHandler = new AudioALSACaptureHandlerVOW(stream_attribute_target);
            status = mCaptureHandler->open();
            if (mDumpReadStart == false) {
                mDumpReadStart = true;
                ret = pthread_create(&hDumyReadThread, NULL, AudioALSAVoiceWakeUpController::dumyReadThread, (void *)this);
                // if pthread_create pass will return 0, otherwise is error message
                if (ret != 0) {
                    ALOGE("%s() create thread fail!!", __FUNCTION__);
                    return UNKNOWN_ERROR;
                }
            }
#ifdef MTK_VOW_BARGE_IN_SUPPORT
            ret = ::ioctl(mFd_vow, VOW_SET_CONTROL, (unsigned int)VOWControlCmd_EnableBargeinDump);
            ALOGD("%s(), EnableBargeinDump set, ret = %d", __FUNCTION__, ret);
            if (ret != 0) {
                ALOGE("%s(), EnableBargeinDump error, ret = %d", __FUNCTION__, ret);
            }
#endif  // #ifdef MTK_VOW_BARGE_IN_SUPPORT
            mDebug_Enable = true;
        }
    } else {
        if (mDebug_Enable) {
            //disable VOW debug dump
            if (mDumpReadStart == true) {
                mDumpReadStart = false;
                pthread_join(hDumyReadThread, NULL);
            }
            status = mCaptureHandler->close();
            delete mCaptureHandler;
#ifdef MTK_VOW_BARGE_IN_SUPPORT
            ret = ::ioctl(mFd_vow, VOW_SET_CONTROL, (unsigned int)VOWControlCmd_DisableBargeinDump);
            ALOGD("%s(), DisableBargeinDump set, ret = %d", __FUNCTION__, ret);
            if (ret != 0) {
                ALOGE("%s(), DisableBargeinDump error, ret = %d", __FUNCTION__, ret);
            }
#endif  // #ifdef MTK_VOW_BARGE_IN_SUPPORT
            mDebug_Enable = false;
        }
    }
    ALOGD("-%s()", __FUNCTION__);
    return NO_ERROR;
}

bool AudioALSAVoiceWakeUpController::getVoiceWakeUpStateFromKernel() {
    ALOGD("%s()+", __FUNCTION__);
    bool bRet = false;
    struct mixer_ctl *ctl;
    ctl = mixer_get_ctl_by_name(mMixer, "Audio_VOW_State");
    bRet = mixer_ctl_get_value(ctl, 0);
    ALOGD("%s(), state = 0x%x", __FUNCTION__, bRet);

    return bRet;
}

#ifdef MTK_VOW_BARGE_IN_SUPPORT
bool AudioALSAVoiceWakeUpController::updateSpeakerPlaybackStatus(bool isSpeakerPlaying) {
    ALOGD("%s(), isSpeakerPlaying = %d", __FUNCTION__, isSpeakerPlaying);
    bool ret = true;
    mIsSpeakerPlaying = isSpeakerPlaying;

    if (getVoiceWakeUpEnable()) {
        if ((isSpeakerPlaying == true) && !mBargeInEnable) {
            ret = setBargeInEnable(true);
        } else if ((isSpeakerPlaying == false) && mBargeInEnable) {
            ret = setBargeInEnable(false);
        }
    }
    return ret;
}

bool AudioALSAVoiceWakeUpController::setBargeInEnable(const bool enable) {
    ALOGD("+%s(), enable = %d", __FUNCTION__, enable);

    int ret;
    if (enable) {
        ASSERT(mBargeInPcm == NULL);

        // open and start barge-in PCM device
        int pcmIndex = 0;
        int cardIndex = 0;
        int pcmStartRet = 0;

        pcmIndex = AudioALSADeviceParser::getInstance()->GetPcmIndexByString(keypcmVOWBargeInCapture);
        cardIndex  = AudioALSADeviceParser::getInstance()->GetCardIndexByString(keypcmVOWBargeInCapture);

        memset((void *)&mBargeInConfig, 0, sizeof(mBargeInConfig));
        mBargeInConfig.channels = 2;
        mBargeInConfig.rate = 16000;
        mBargeInConfig.format = PCM_FORMAT_S16_LE;

        const int interruptIntervalMs = 10; // Interrupt rate from AP to SCP is 10ms
        //mBargeInConfig.period_size = 640;
        mBargeInConfig.period_size = interruptIntervalMs * mBargeInConfig.rate / 1000;
        mBargeInConfig.period_count = 4; // Allocate 40ms DMA buffer

        mBargeInConfig.start_threshold = 0;
        mBargeInConfig.stop_threshold = 0;
        mBargeInConfig.silence_threshold = 0;

        mBargeInPcm = pcm_open(cardIndex, pcmIndex, PCM_IN, &mBargeInConfig);
        if (mBargeInPcm == NULL) {
            ALOGE("%s(), mBargeInPcm == NULL!! mBargeInPcm = %p, pcmIndex = %d, cardIndex = %d", __FUNCTION__, mBargeInPcm, pcmIndex, cardIndex);
        } else if (pcm_is_ready(mBargeInPcm) == false) {
            ALOGE("%s(), pcm_is_ready(%p) == false due to %s, close pcm.", __FUNCTION__, mBargeInPcm, pcm_get_error(mBargeInPcm));
            pcm_close(mBargeInPcm);
            mBargeInPcm = NULL;
        } else {
            pcmStartRet = pcm_start(mBargeInPcm);
        }

        ALOGD("-%s(), mBargeInPcm = %p, pcmStartRet = %d", __FUNCTION__, mBargeInPcm, pcmStartRet);
        ASSERT(mBargeInPcm != NULL);
        mBargeInEnable = true;
        ret = ::ioctl(mFd_vow, VOW_SET_CONTROL, (unsigned int)VOW_BARGEIN_ON);
        ALOGD("%s(), VOW_BARGEIN_ON set, ret = %d", __FUNCTION__, ret);
        if (ret != 0) {
            ALOGE("%s(), VOW_BARGEIN_ON error, ret = %d", __FUNCTION__, ret);
        }
    } else {
        if (mBargeInPcm != NULL) {
            ret = ::ioctl(mFd_vow, VOW_SET_CONTROL, (unsigned int)VOW_BARGEIN_OFF);
            ALOGD("%s(), VOW_BARGEIN_OFF set, ret = %d", __FUNCTION__, ret);
            if (ret != 0) {
                ALOGE("%s(), VOW_BARGEIN_OFF error, ret = %d", __FUNCTION__, ret);
            }
            pcm_stop(mBargeInPcm);
            pcm_close(mBargeInPcm);
            mBargeInPcm = NULL;
            mBargeInEnable = false;
        }

        ALOGD("-%s(), mBargeInPcm = %p", __FUNCTION__, mBargeInPcm);
    }
    return true;
}
#endif

} // end of namespace android
