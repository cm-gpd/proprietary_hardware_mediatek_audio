#ifndef ANDROID_AUDIO_DSP_STREAM_MANAGER_H
#define ANDROID_AUDIO_DSP_STREAM_MANAGER_H

#include <utils/threads.h>
#include <utils/KeyedVector.h>
#ifdef MTK_BASIC_PACKAGE
#include "AudioTypeExt.h"
#endif

#include <hardware_legacy/AudioMTKHardwareInterface.h>

#include <tinyalsa/asoundlib.h>
#include "AudioType.h"
#include "AudioLock.h"
#include "AudioPolicyParameters.h"
#include "AudioDspType.h"


#ifdef MTK_AURISYS_FRAMEWORK_SUPPORT
struct aurisys_lib_manager_t;
struct aurisys_dsp_config_t;
struct aurisys_gain_config_t;
#endif


namespace android {

class AudioALSAPlaybackHandlerBase;
class AudioALSACaptureHandlerBase;

class AudioDspStreamManager {
public:
    virtual ~AudioDspStreamManager();
    static AudioDspStreamManager *getInstance();

    int addPlaybackHandler(AudioALSAPlaybackHandlerBase *playbackHandler);
    int removePlaybackHandler(AudioALSAPlaybackHandlerBase *playbackHandler);

    int addCaptureHandler(AudioALSACaptureHandlerBase *captureHandler);
    int removeCaptureHandler(AudioALSACaptureHandlerBase *captureHandler);

    int dumpPlaybackHandler(void);
    int dumpCaptureHandler(void);

    unsigned int getUlLatency(void);
    unsigned int getDlLatency(void);
    bool getDspTaskPlaybackStatus(void);

    int setAfeInDspShareMem(bool condition);
    int setAfeOutDspShareMem(unsigned int flag, bool condition);
    int setStreamInState(bool condition);
    int setStreamOutState(unsigned int flag, bool condition);


#ifdef MTK_AURISYS_FRAMEWORK_SUPPORT
    void CreateAurisysLibManager(
        struct aurisys_lib_manager_t **manager,
        struct aurisys_dsp_config_t **config,
        const uint8_t task_scene,
        const uint32_t aurisys_scenario,
        const uint8_t arsi_process_type,
        const uint32_t audio_mode,
        const struct stream_attribute_t *attribute_in,
        const struct stream_attribute_t *attribute_out,
        const struct stream_attribute_t *attribute_ref,
        const struct aurisys_gain_config_t *gain_config);
    void DestroyAurisysLibManager(
        struct aurisys_lib_manager_t **manager,
        struct aurisys_dsp_config_t **config,
        const uint8_t task_scene);
#endif


private:
    /**
     * singleton pattern
     */
    AudioDspStreamManager();
    static AudioDspStreamManager *mDspStreamManager;

    /*
     *  function to set pcm config and open pcmDumpThreadCreated
     */
    int checkPlaybackStatus(void);
    int startPlaybackTask(AudioALSAPlaybackHandlerBase *playbackHandler);
    int stopPlaybackTask(void);
    int triggerDsp(unsigned int task_scene, int data_type);

    bool dataPasstoDsp(AudioALSAPlaybackHandlerBase *Base);

    int setAfeDspShareMem(bool condition);
    int doRecoveryState();
    int setStreamState(bool condition);


    /**
     * stream manager lock
     */
    AudioLock mLock;

    /**
     * stream playback/capture handler vector
     */
    KeyedVector<unsigned long long, AudioALSAPlaybackHandlerBase *> mPlaybackHandlerVector;
    KeyedVector<unsigned long long, AudioALSACaptureHandlerBase *>  mCaptureHandlerVector;

    /*
     * pcm for capture pcm and playback pcmDumpThreadCreated
     */
    struct pcm_config mPlaybackUlConfig;
    struct pcm *mPlaybackUlPcm;
    int mPlaybackUlindex;
    struct pcm_config mPlaybackDlConfig;
    struct pcm *mPlaybackDlPcm;
    int mPlaybackDlindex;
    struct pcm_config mDspConfig;
    struct pcm *mDspPcm;
    int mDspIndex;
    bool mDspTaskPlaybackActive;
    unsigned int multiplier;

    /* dsp stream state */
    int mDspStreamState;
    int mStreamCardIndex;

    /* dsp ap interconnection to device */
    String8 mApTurnOnSequence;
    String8 mApTurnOnSequence2;

    /* Mixer*/
    struct mixer *mMixer;


#ifdef MTK_AURISYS_FRAMEWORK_SUPPORT
    struct aurisys_lib_manager_t *mAurisysLibManagerPlayback;
    struct aurisys_dsp_config_t *mAurisysDspConfigPlayback;
#endif
};

} // end namespace android

#endif // end of ANDROID_AUDIO_DSP_STREAM_MANAGER_H
